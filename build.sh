#!/usr/bin/env sh

file=statement.tex

json=$(curl -XPOST -F "file=@$file" "https://inspirehep.net/api/bibliography-generator?format=bibtex")

errors=$(echo $json | grep -Po '"errors":\[.*?\]' | cut -c 10-)

if [ "$errors" != "[]" ]; then
	echo "Errors: $errors"
	exit 1
else
	echo "No errors"
fi

url=$(echo $json | perl -pe 's/.*"download_url":"(.*?)".*/\1/g')

curl $url > statement.bib

latexmk -pdf $file
