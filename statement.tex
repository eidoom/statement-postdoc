\documentclass[a4paper]{article}

\usepackage[colorlinks,linkcolor=blue,urlcolor=blue,citecolor=blue]{hyperref}
\usepackage[style=numeric-comp,sorting=none]{biblatex}
\usepackage{cleveref}

\pagenumbering{gobble}

\addbibresource{statement.bib}

\title{Research statement}
\author{Ryan Moodie}
\date{}

\begin{document}

\maketitle

My PhD research is focussed on the calculation of quantum chromodynamic (QCD) amplitudes at the precision frontier.
This presents the challenge of handling the algebraic and analytic complexity of higher-order perturbative expressions~\cite{Tancredi:2021oiq}.
Furthermore, assembly of observables from these contributions requires careful regulation of infrared (IR) behaviour to cancel poles at each order.
I work within Simon Badger's \href{http://personalpages.to.infn.it/~badger/jetdynamics/}{JetDynamics group} to implement analytical expressions of these amplitudes into the public \texttt{C++} library \texttt{NJet}~\cite{Badger:2012pg}, available at \url{https://bitbucket.org/njet/njet}.
The results provide a vital ingredient for theoretical predictions of cross sections at the Large Hadron Collider in the search for deviations from the Standard Model (SM)~\cite{Gehrmann:2021qex}.

\section*{Infrared limits}

I am extending \texttt{NJet} to include a library of the various soft and collinear limit functions of partonic amplitudes.
These limits will provide the building blocks of counterterms to regulate IR divergences at next-to-next-to-leading-order (NNLO)~\cite{TorresBobadilla:2020ekr,Magnea:2020trj,Heinrich:2020ybq}.

They also offer a convenient way to validate new higher-order expressions.
This proceeds by comparison of numerical evaluation of the full amplitude in IR limits of phase space to the factorised product of limit functions and lower-multiplicity ``reduced'' amplitudes through a momentum mapping scheme.

\section*{Amplitude neural networks for gluon fusion to diphoton plus jets}

The gluon-initiated diphoton amplitudes offer an attractive testing ground for novel technology as they are loop-induced, presenting challenges for conventional cross section techniques.
They are also a relevant background for interesting phenomenology such as probing the Higgs coupling~\cite{Caola:2015wna}.
Therefore, I added numerical implementations of them for multiplicities four to six into \texttt{NJet} by summing over permutations of the existing all-gluon processes as in Ref.~\cite{deFlorian:1999tp}.

Finding numerical performance to scale poorly with multiplicity, we obtained analytic expressions for gluon fusion to diphoton plus jet at one loop.
First, we calculated the amplitude using the permutation sum trick.
We then obtained efficient analytical expressions, in particular using a rational parametrisation of the external kinematics through momentum twistors~\cite{Hodges:2009hk} and reconstruction over finite fields~\cite{Peraro:2019svx}, to bypass intermediate algebraic complexity.
Finally, we validated against the previous result.
The analytic result was orders of magnitude faster.

For the six-point process ($gg\to\gamma\gamma gg$), it seems even efficient analytical expressions may not provide desirable evaluation times.
Therefore, we studied the feasibility of using neural networks to optimise the evaluation of the matrix element for cross section calculations, following related work for $e^+e^-$ colliders~\cite{Badger:2020uow}.
We used the analytical five-point result ($gg\to\gamma\gamma g$) to train a neural network ensemble, fed it into the Monte Carlo event generator Sherpa~\cite{Bothmann:2019yzt}, then investigated its behaviour within a full hadronic simulation.
Learning from this, we trained a new model on the six-point numerical implementation and demonstrated its use in cross section calculations, finding a speedup of a factor of 30 in the total simulation time~\cite{Aylett-Bullock:2021hmo}.
This method offers a performant way to run high multiplicity contributions, in particular radiative corrections, for event generator simulations where conventional techniques are prohibitively slow.

\section*{Two-loop five-point full-colour amplitudes for phenomenology}

Modern collider experiments demand increasing precision from theoretical predictions, pushing for NNLO contributions to $2\to3$ processes.
Such ``industrialised'' analytical computations of two-loop five-point QCD processes present huge technical challenges.
We first need a basis of special functions offering fast and stable evaluation over phase space;
for massless and single-mass scattering, the pentagon functions library~\cite{Chicherin:2020oor,Chicherin:2021dyp} has recently become available.
Generating the amplitude via colour-ordered diagrams, we reduce tensor integrals to a master integral basis which can be expressed in terms of these special functions.
This requires constructing and solving a very large system of integration-by-parts identities.
Finally, we obtain the coefficients in an efficient form using on-shell amplitude methods~\cite{Berger:2009zb}.

Our work is at the forefront of tackling these challenges and making the results ready for immediate phenomenological application.
Using the pentagon functions for the special function basis, we have been working on implementing the coefficients within \texttt{NJet} to construct the 't Hooft-Veltman finite remainders for various processes.
These generally provide the double virtual corrections to NNLO cross sections.
We use direct analytic reconstruction of the rational coefficients over finite fields of special functions.

We computed the two-loop leading-colour five-point amplitudes for hadronic triple jet production and are working on implementing them within \texttt{NJet}.

For $gg\to\gamma\gamma g$, we completed and released through \texttt{NJet} the full-colour two-loop process~\cite{Badger:2021imn}.
While the colour structure is simpler than the purely partonic case, the leading-colour contribution contains non-planar integrals, which are those with the highest complexity.
We used the antenna subtraction scheme to integrate this amplitude to obtain differential cross sections~\cite{Badger:2021ohm}.
For $pp\to\gamma\gamma j$, this subprocess enters at next-to-next-to-next-to-leading-order (N$^3$LO) but the large gluonic parton distribution function means it provides a dominant correction to NNLO.

\section*{Future research}

Moving forward, I am interested to continue leveraging novel techniques in mathematics and computation to tackle the challenges that face precision tests of the SM today.
To help bring theoretical predictions into the realm of percent precision, we need to further map out the phase space of NNLO processes within fixed-order perturbation theory.
In particular, in order for these to be applicable to phenomenology, it is vital for their implementations to be optimally efficient and stable.
Exploring this problem is the focus of my near-term future research.
I am also actively open to the many other aspects of precision phenomenology and would be excited to contribute to the field from different angles too.

\printbibliography

\end{document}
